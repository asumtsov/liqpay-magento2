<?php
/**
 * Magedone
 *
 * This source file is subject to the Magedone Software License, which is available at http://magedone.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magento.com for more information.
 *
 * @category  Magedone
 * @package   Liqpay
 * @version   1.0.0
 * @copyright Copyright (C) 2017 Magedone (http://magedone.com/)
 */

namespace Magedone\Liqpay\Controller\Payment;

use \Magento\Framework\App\Action\Action;

use \Magento\Framework\App\Action\Context;
use \Psr\Log\LoggerInterface;
use \Magento\Framework\Session\SessionManagerInterface;
use \Magedone\Liqpay\Helper\Data;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Controller\ResultFactory;
use \Magedone\Liqpay\Model\PaymentMethod;

/**
 * Class AbstractAction Abstract Action class for Liqpay, that stores common methods.
 * @package Magedone\Liqpay\Controller\Payment
 */
abstract class AbstractAction extends Action
{
    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Checkout session
     *
     * @var SessionManagerInterface
     */
    protected $session;

    /**
     * Result page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $resultPageFactory;

    /**
     * Result factory
     *
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $rawResultFactory;

    /**
     * Liqpay helper
     *
     * @var \Magedone\Liqpay\Helper\Data
     */
    protected $helper;

    /**
     * Payment method object
     *
     * @var \Magedone\Liqpay\Model\PaymentMethod
     */
    protected $paymentMethod;

    /**
     * General constructor for module actions
     *
     * @param Context $context
     * @param LoggerInterface $logger
     * @param SessionManagerInterface $session
     * @param Data $helper
     * @param PageFactory $resultPageFactory
     * @param ResultFactory $rawResultFactory
     * @param PaymentMethod $paymentMethod
     */
    public function __construct(
        Context $context,
        LoggerInterface $logger,
        SessionManagerInterface $session,
        Data $helper,
        PageFactory $resultPageFactory,
        ResultFactory $rawResultFactory,
        PaymentMethod $paymentMethod
    ) {
        parent::__construct($context);
        $this->logger = $logger;
        $this->session = $session;
        $this->resultPageFactory = $resultPageFactory;
        $this->rawResultFactory = $rawResultFactory;
        $this->helper = $helper;
        $this->paymentMethod = $paymentMethod;

    }

    /**
     * Return Order model
     *
     * @param string|null $orderId
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder($orderId = null)
    {
        return $this->helper->getOrder($orderId);
    }

}