<?php
/**
 * Magedone
 *
 * This source file is subject to the Magedone Software License, which is available at http://magedone.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magento.com for more information.
 *
 * @category  Magedone
 * @package   Liqpay
 * @version   1.0.0
 * @copyright Copyright (C) 2017 Magedone (http://magedone.com/)
 */

namespace Magedone\Liqpay\Controller\Payment;

/**
 * Class Redirect
 */
class Redirect extends AbstractAction
{

    /**
     * Action for order data processing before sending to Liqpay
     *
     * @return void|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $quoteId = $this->session->getLastQuoteId();
        $lastRealOrderId = $this->session->getLastRealOrderId();

        if ($quoteId === null || $lastRealOrderId === null) {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('checkout/cart/');
            return $resultRedirect;
        } else {
            $this->session->setLiqpayQuoteId($quoteId);
            $this->session->setLiqpayLastRealOrderId($lastRealOrderId);
            $order = $this->getOrder();

            $order->addStatusToHistory(
                $order->getStatus(),
                __('Customer switch over to Liqpay payment interface.')
            );
            $this->helper->saveOrder($order);

            $this->session->getQuote()->setIsActive(false)->save();
            $this->session->setQuoteId(null);
            $this->session->setLastRealOrderId(null);

            $resultPage = $this->resultPageFactory ->create();
            return $resultPage;
        }
    }
}
