<?php
/**
 * Magedone
 *
 * This source file is subject to the Magedone Software License, which is available at http://magedone.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magento.com for more information.
 *
 * @category  Magedone
 * @package   Liqpay
 * @version   1.0.0
 * @copyright Copyright (C) 2017 Magedone (http://magedone.com/)
 */

namespace Magedone\Liqpay\Controller\Payment;

/**
 * Class Result
 */
class Result extends AbstractAction
{

    /**
     * Redirecting after Liqpay checkout handling.
     *
     * @return void|bool
     */
    public function execute()
    {
        $orderId = $this->session->getLiqpayLastRealOrderId();
        $quoteId = $this->session->getLiqpayQuoteId(true);

        $order = $this->getOrder($orderId);

        if ($order->isEmpty()) {
            return false;
        }

        if ($this->paymentMethod->isSandbox()) {
            $order->addStatusHistoryComment(
                __('Invoice was placed in sandbox mode.')
            );
        }

        $order->addStatusHistoryComment(
            __('Customer successfully got back from Liqpay payment interface.')
        );

        $this->helper->saveOrder($order);
        $this->session->setQuoteId($quoteId);
        $this->session->getQuote()->setIsActive(false)->save();
        $this->session->setLastRealOrderId($orderId);

        $this->_redirect('checkout/onepage/success', ['_secure' => true]);
    }
}
