/**
 * Magedone
 *
 * This source file is subject to the Magedone Software License, which is available at http://magedone.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magento.com for more information.
 *
 * @category  Magedone
 * @package   Liqpay
 * @version   1.0.0
 * @copyright Copyright (C) 2017 Magedone (http://magedone.com/)
 */

define(
    [
        'Magento_Checkout/js/view/payment/default',
        'mage/url',
    ],
    function (
        Component,
        url) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magedone_Liqpay/payment/liqpay'
            },
            getTitle: function () {
                return this.item.title
            },

            getDescription: function () {
                return window.checkoutConfig.payment[this.getCode()].description;
            },

            getCode: function () {
                return 'liqpay';
            },
            afterPlaceOrder: function () {
                this.redirectAfterPlaceOrder = false;
                window.location.replace(url.build('liqpay/payment/redirect/'));
            }
        });
    }
);