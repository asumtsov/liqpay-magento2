<?php
/**
 * Magedone
 *
 * This source file is subject to the Magedone Software License, which is available at http://magedone.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magento.com for more information.
 *
 * @category  Magedone
 * @package   Liqpay
 * @version   1.0.0
 * @copyright Copyright (C) 2017 Magedone (http://magedone.com/)
 */

namespace Magedone\Liqpay\Block;

use \Magento\Framework\View\Element\Template\Context;
use \Magedone\Liqpay\Model\PaymentMethod;

/**
 * Gather data and sends it to Liqpay using form submit.
 *
 * Class Redirect
 * @package Magedone\Liqpay\Block
 */
class Redirect extends \Magento\Framework\View\Element\Template
{
    /**
     * Payment method model
     *
     * @var \Magedone\Liqpay\Model\PaymentMethod
     */
    private $paymentMethod;

    /**
     * Redirect block constructor creates instances of necessary classes
     *
     * @param Context $context
     * @param PaymentMethod $paymentMethod
     * @param array $data
     */
    public function __construct(
        Context $context,
        PaymentMethod $paymentMethod,
        array $data = []
    ) {
        $this->paymentMethod = $paymentMethod;
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
    }

    /**
     * Set template for this block
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->setTemplate('liqpay/redirect.phtml');

        return parent::_prepareLayout();
    }

    /**
     * Return url link for data sending
     *
     * @return string
     */
    public function getPlacementUrl()
    {
        return $this->paymentMethod->getLiqpayPlaceUrl();
    }

    /**
     * Return redirect from data from payment method model
     *
     * @return \string[]
     */
    public function getFormFields()
    {
        return $this->paymentMethod->getRedirectFormFields();
    }
}