<?php
/**
 * Magedone
 *
 * This source file is subject to the Magedone Software License, which is available at http://magedone.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magento.com for more information.
 *
 * @category  Magedone
 * @package   Liqpay
 * @version   1.0.0
 * @copyright Copyright (C) 2017 Magedone (http://magedone.com/)
 */

namespace Magedone\Liqpay\Block;

class Info extends \Magento\Payment\Block\Info
{
    /**
     * Liqpay payment method title template for admin
     *
     * @var string
     */
    protected $_template = 'Magedone_Liqpay::liqpay/info.phtml';
} 