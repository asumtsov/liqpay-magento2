<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magedone\Liqpay\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use \Magento\Payment\Gateway\Config\Config;


/**
 * Class ConfigProvider
 */
final class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'liqpay';

    /**
     * Stores Payment config object
     *
     * @var Config
     */
    private $config;

    /**
     * Constructor, loading of payment config into this Config Provider
     *
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
        $this->config ->setMethodCode(self::CODE);
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                self::CODE => [
                    'isActive' => $this->isActive(),
                    'description' => $this->config->getValue('description')
                ]
            ]
        ];

    }

    /**
     * Get Payment configuration status
     *
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->config->getValue('active');
    }
}
