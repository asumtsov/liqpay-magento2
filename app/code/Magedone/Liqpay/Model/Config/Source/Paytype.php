<?php
/**
 * Magedone
 *
 * This source file is subject to the Magedone Software License, which is available at http://magedone.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magento.com for more information.
 *
 * @category  Magedone
 * @package   Liqpay
 * @version   1.0.0
 * @copyright Copyright (C) 2017 Magedone (http://magedone.com/)
 */


namespace Magedone\Liqpay\Model\Config\Source;

/**
 * Class Paytype source model for liqpay paytypes
 * @package Magedone\Liqpay\Model\Config\Source
 */
class Paytype
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'card', 'label' => __('Credit Card (Visa/MasterCard)')],
            ['value' => 'liqpay', 'label' => __('Liqpay')],
            ['value' => 'privat24', 'label' => __('Privat24')],
            ['value' => 'masterpass', 'label' => __('Masterpass')],
            ['value' => 'moment_part', 'label' => __('Moment part')],
            ['value' => 'cash', 'label' => __('Cash')],
            ['value' => 'invoice', 'label' => __('Invoice to email')],
            ['value' => 'qr', 'label' => __('QR')],
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'card' => __('Credit Card (Visa/MasterCard)'),
            'liqpay' => __('Liqpay'),
            'privat24' => __('Privat24'),
            'masterpass' => __('Masterpass'),
            'moment_part' => __('Moment part'),
            'cash' => __('Cash'),
            'invoice' => __('Invoice to email'),
            'qr' => __('QR'),
        ];
    }
} 