<?php
/**
 * Magedone
 *
 * This source file is subject to the Magedone Software License, which is available at http://magedone.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magento.com for more information.
 *
 * @category  Magedone
 * @package   Liqpay
 * @version   1.0.0
 * @copyright Copyright (C) 2017 Magedone (http://magedone.com/)
 */
namespace Magedone\Liqpay\Model\Config\Source;

use Magento\Directory\Model\ResourceModel\Country\Collection;

class Country implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Variable for options caching inside object
     *
     * @var array
     */
    protected $options;

    /**
     * Countries
     *
     * @var \Magento\Directory\Model\ResourceModel\Country\Collection
     */
    protected $countryCollection;

    /**
     * Countries not supported by Liqpay
     *
     * @var array
     */
    protected $excludedCountries = [];

    /**
     * Country source model country loading
     *
     * @param Collection $countryCollection
     */
    public function __construct(Collection $countryCollection)
    {
        $this->countryCollection = $countryCollection;
    }

    /**
     * Country options for Liqpay payment method work in
     *
     * @param bool $isMultiselect
     * @return array
     */
    public function toOptionArray($isMultiselect = false)
    {
        if (!$this->options) {
            $this->options = $this->countryCollection
                //->addFieldToFilter('country_id', ['nin' => $this->getExcludedCountries()])
                ->loadData()
                ->toOptionArray(false);
        }

        $options = $this->options;
        if (!$isMultiselect) {
            array_unshift($options, ['value' => '', 'label' => __('--Please Select--')]);
        }

        return $options;
    }

    /**
     * If country is in list of restricted (not supported by Liqpay)
     *
     * @param string $countryId
     * @return boolean
     */
    public function isCountryRestricted($countryId)
    {
        return in_array($countryId, $this->getExcludedCountries());
    }

    /**
     * Return list of excluded countries
     * @return array
     */
    public function getExcludedCountries()
    {
        return $this->excludedCountries;
    }
} 