<?php
/**
 * Magedone
 *
 * This source file is subject to the Magedone Software License, which is available at http://magedone.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magento.com for more information.
 *
 * @category  Magedone
 * @package   Liqpay
 * @version   1.0.0
 * @copyright Copyright (C) 2017 Magedone (http://magedone.com/)
 */

namespace Magedone\Liqpay\Model;

use \Magento\Framework\Model\Context;
use \Magento\Framework\Registry;
use \Magento\Framework\Api\ExtensionAttributesFactory;
use \Magento\Framework\Api\AttributeValueFactory;
use \Magento\Payment\Helper\Data as PaymentData;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Payment\Model\Method\Logger;
use \Magedone\Liqpay\Helper\Data;
use \Magento\Framework\UrlInterface;
use \Magento\Framework\Model\ResourceModel\AbstractResource;
use \Magento\Framework\Data\Collection\AbstractDb;
use \Magento\Framework\Exception\PaymentException;

/**
 * Pay In Store payment method model
 *
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 *
 */
class PaymentMethod extends \Magento\Payment\Model\Method\AbstractMethod
{

    const STATUS_SUCCESS     = 'success';
    const STATUS_FAILURE     = 'failure';
    const STATUS_WAIT_SECURE = 'wait_secure';
    const STATUS_WAIT_ACCEPT = 'wait_accept';
    const STATUS_SANDBOX     = 'sandbox';

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canCapture             = true;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canVoid                = true;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canUseForMultishipping = false;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canUseInternal         = false;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_isInitializeNeeded     = true;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_isGateway               = false;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canAuthorize            = false;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canCapturePartial       = false;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canRefund               = false;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canRefundInvoicePartial = false;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canUseCheckout          = true;

    /**
     * Helper instance in order to get Order objects
     *
     * @var \Magedone\Liqpay\Helper\Data
     */
    private $helper;

    /**
     * Url instance
     *
     * @var \Magento\Framework\UrlInterface
     */
    private $urlInstance;

    /**
     * Info block type for displaying in admin
     *
     * @var string
     */
    protected $_infoBlockType = \Magedone\Liqpay\Block\Info::class;

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'liqpay';

    /**
     * Order model
     *
     * @var \Magento\Sales\Api\Data\InvoiceInterface
     */
    private $order;

    /**
     * Payment method constructor creates instances of necessary classes
     *
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param PaymentData $paymentData
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param Data $helper
     * @param UrlInterface $urlInstance
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        PaymentData $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        Data $helper,
        UrlInterface $urlInstance,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );

        $this->helper = $helper;

        $this->urlInstance = $urlInstance;
    }

    /**
     * Return encrypted value from config by path
     *
     * @param string $path
     * @return string
     */
    private function getEncryptedConfigData($path)
    {
        $rawValue = $this->getConfigData($path);
        return $this->helper->decrypt($rawValue);
    }

    /**
     * Return decrypted public key for Liqpay
     *
     * @return string
     */
    public function getPublicKey()
    {
        return $this->getEncryptedConfigData('public_key');
    }

    /**
     * Return decrypted private key for Liqpay
     *
     * @return string
     */
    public function getPrivateKey()
    {
        return $this->getEncryptedConfigData('private_key');
    }

    /**
     * Return array of params for Liqpay request
     *
     * @return string[]
     */
    public function getRedirectFormFields()
    {
        $order = $this->helper->getOrder();

        if (!$order->getId()) {
            return [];
        }

        $privateKey = $this->getPrivateKey();
        $publicKey  = $this->getPublicKey();

        $amount      = $order->getGrandTotal();
        $currency    = $order->getOrderCurrencyCode();
        if ($currency == 'RUR') {
            $currency = 'RUB';
        }

        $orderId    = $order->getIncrementId();
        $description = __('Order') . ' №' . $orderId;

        $resultUrl  = $this->urlInstance->getUrl('liqpay/payment/result');
        $serverUrl  = $this->urlInstance->getUrl('liqpay/payment/server');

        $action      = 'pay';
        $version     = '3';
        $language    = $this->getConfigData('language');
        $request = [
            'version'     => $version,
            'public_key'  => $publicKey,
            'amount'      => $amount,
            'currency'    => $currency,
            'description' => $description,
            'order_id'    => $orderId,
            'action'      => $action,
            'language'    => $language,
            'result_url'  => $resultUrl,
            'server_url'  => $serverUrl
        ];

        if ($this->isSandbox()) {
            $request['sandbox'] = 1;
        }

        $this->_debug([
            'url' => $this->getLiqpayPlaceUrl(),
            'request' => $request
        ]);

        $data = base64_encode(json_encode($request));

        $signature = base64_encode(sha1($privateKey . $data . $privateKey, 1));

        return compact('data', 'signature');
    }

    /**
     * Return Liqpay place URL
     *
     * @return string
     */
    public function getLiqpayPlaceUrl()
    {
        return $this->getConfigData('url');
    }

    /**
     * Check sandbox mode for extension
     *
     * @return bool
     */
    public function isSandbox()
    {
        return ($this->getConfigData('environment') == 'sandbox');
    }

    /**
     * Handles request accepted from Liqpay server and creates invoice
     *
     * @param \Zend\Stdlib\Parameters $post
     * @throws PaymentException
     * @return void
     */
    public function processNotification($post)
    {
        $this->_debug([
            'response' => $post
        ]);

        $success =
            isset($post['data']) &&
            isset($post['signature']);

        if (!$success) {
            throw new PaymentException (__('Data or signature is empty'));
        }

        $data         = $post['data'];
        $decodedData = base64_decode($data);

        $this->_debug([
            'decoded_data' => $decodedData
        ]);

        $parsedData = json_decode($decodedData, true, 1024);

        $this->_debug([
            'parsed_response' => $parsedData
        ]);

        $receivedSignature = $post['signature'];
        $receivedPublicKey = $parsedData['public_key'];
        $orderId           = $parsedData['order_id'];
        $status            = $parsedData['status'];
        $amount            = $parsedData['amount'];
        $currency          = $parsedData['currency'];
        $transactionId     = $parsedData['transaction_id'];

        if ($orderId <= 0) {
            throw new PaymentException (__('Order id is not set'));
        }

        $this->order = $this->helper->getOrder($orderId);

        if (!$this->order->getId()) {
            throw new PaymentException (__('Cannot load order'));
        }

        $privateKey = $this->getPrivateKey();
        $publicKey  = $this->getPublicKey();

        $generatedSignature = base64_encode(sha1($privateKey . $data . $privateKey, 1));

        if ($receivedSignature != $generatedSignature || $publicKey != $receivedPublicKey) {
            $this->order->addStatusHistoryComment(__('Security check failed!'));
            $this->helper->saveOrder($this->order);
            return;
        }

        $newOrderStatus = $this->getConfigData('order_status', $this->order->getStoreId());
        if (empty($newOrderStatus)) {
            $newOrderStatus = $this->order->getStatus();
        }
        $this->order->setStatus($newOrderStatus);

        $this->_debug([
            'response_status' => $status
        ]);

        if (in_array($status, [self::STATUS_SANDBOX, self::STATUS_SUCCESS])) {
            $this->processSuccessNotification($transactionId, $amount, $currency);
        } else {
            $this->processUnsuccessNotification($status);
        }

        $this->helper->saveOrder($this->order);
    }

    /**
     * Processing success or sandbox
     *
     * @param string $transactionId
     * @param string $amount
     * @param string $currency
     *
     * @return void
     */
    private function processSuccessNotification(
        $transactionId,
        $amount,
        $currency
    ) {
        if ($this->order->canInvoice()) {
            $this->order->getPayment()->setTransactionId($transactionId);
            $invoice = $this->order->prepareInvoice();
            $invoice->register()->pay();
            $this->helper->saveInvoice($invoice);
            $this->helper->saveOrder($this->order);

            if (!$this->isSandbox()) {
                $message = __(
                    'Invoice #%1 created.',
                    [$invoice->getIncrementId()]
                );
            } else {
                $message = __(
                    'Invoice #%1 created (sandbox).',
                    $invoice->getIncrementId()
                );
            }
            $notified = true;
            $this->helper->setOrderState(
                $this->order,
                \Magento\Sales\Model\Order::STATE_PROCESSING,
                $message,
                $notified
            );

            $sDescription = '';
            $sDescription .= 'amount: ' . $amount . '; ';
            $sDescription .= 'currency: ' . $currency . '; ';

            $this->order->addStatusHistoryComment($sDescription)
                ->setIsCustomerNotified($notified);

        } else {
            $this->order->addStatusHistoryComment(__('Error during creation of invoice.'))
                ->setIsCustomerNotified($notified = true);
        }
    }

    /**
     * Sets order status if server sent not successful answer
     *
     * @param string $status
     *
     * @throws PaymentException
     *
     * @return void
     */
    private function processUnsuccessNotification($status)
    {
        $statusMap = [
            self::STATUS_FAILURE => [
                'state' => \Magento\Sales\Model\Order::STATE_CANCELED,
                'message' => __('Liqpay error.'),
            ],
            self::STATUS_WAIT_SECURE => [
                'state' => \Magento\Sales\Model\Order::STATE_PROCESSING,
                'message' => __('Waiting for verification from the Liqpay side.'),
            ],
            self::STATUS_WAIT_ACCEPT => [
                'state' => \Magento\Sales\Model\Order::STATE_PROCESSING,
                'message' => __('Waiting for accepting from the buyer side.'),
            ]

        ];
        if (isset($statusMap[$status])) {
            $this->helper->setOrderState(
                $this->order,
                $statusMap[$status]['state'],
                $statusMap[$status]['message'],
                true

            );
        } else {
            new PaymentException(__('Unexpected status from server: %l', [$status]));
        };
    }
}