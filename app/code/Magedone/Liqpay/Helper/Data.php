<?php
/**
 * Magedone
 *
 * This source file is subject to the Magedone Software License, which is available at http://magedone.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magento.com for more information.
 *
 * @category  Magedone
 * @package   Liqpay
 * @version   1.0.0
 * @copyright Copyright (C) 2017 Magedone (http://magedone.com/)
 */
namespace Magedone\Liqpay\Helper;

use Magento\Framework\Session\SessionManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use \Magento\Sales\Api\InvoiceRepositoryInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Sales\Model\Service\OrderService;
/**
 * Class Data is a helper for working with orders.
 * @package Magedone\Liqpay\Helper
 */
class Data
{
    /**
     * Order model
     *
     * @var \Magento\Sales\Model\Order
     */
    private $order;

    /**
     * Search criteria builder for looking order
     *
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * Checkout session
     *
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    private $session;

    /**
     * Order repository service contract
     *
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Invoice repository for invoice saving
     *
     * @var \Magento\Sales\Api\InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * Encryptor instance in order to work with encrypted keys
     *
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    private $encryptor;

    /**
     * Object for manipulating order model state
     *
     * @var \Magento\Sales\Model\Service\OrderService
     */
    private $orderService;

    /**
     * Helper constructor creates instances of necessary classes
     *
     * @param SessionManagerInterface $session
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param EncryptorInterface $encryptor
     * @param OrderService $orderService
     */
    public function __construct(
        SessionManagerInterface $session,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        InvoiceRepositoryInterface $invoiceRepository,
        EncryptorInterface $encryptor,
        OrderService $orderService
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->session = $session;
        $this->invoiceRepository = $invoiceRepository;
        $this->encryptor = $encryptor;
        $this->orderService = $orderService;
    }

    /**
     * Return model of order that increment_id is given in param or last customer created order
     *
     * @param string|null $orderIncrementId
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder($orderIncrementId = null)
    {
        if ($this->order === null) {
            if ($orderIncrementId === null) {
                $orderIncrementId = $this->session->getLastRealOrderId();
            }
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter('increment_id', $orderIncrementId, 'eq')->create();
            $orderList = $this->orderRepository->getList($searchCriteria);
            $orderList = $orderList->getItems();
            $this->order = reset($orderList);
        }

        return $this->order;
    }

    /**
     * Saves order
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return void
     */
    public function saveOrder(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $this->orderRepository->save($order);
    }

    /**
     * Save invoice
     *
     * @param \Magento\Sales\Api\Data\InvoiceInterface $invoice
     * @return void
     */
    public function saveInvoice(\Magento\Sales\Api\Data\InvoiceInterface $invoice)
    {
        $this->invoiceRepository->save($invoice);
    }

    /**
     * Set order state
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param string $state
     * @param string $message
     * @param bool $notified
     *
     * @return void
     */
    public function setOrderState($order, $state, $message, $notified)
    {
        $this->orderService->setState(
            $order,
            $state,
            true,
            $message,
            $notified,
            false
        );
    }

    /**
     * Decrypt string
     *
     * @param string $value
     * @return string
     */
    public function decrypt($value)
    {
        return $this->encryptor->decrypt($value);
    }

} 